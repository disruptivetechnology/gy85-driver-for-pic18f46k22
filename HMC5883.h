/* 
 * File:   HMC5883.h
 * Author: eluinstra
 *
 * Created on February 10, 2016, 7:49 PM
 */

#ifndef HMC5883_H
#define	HMC5883_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/i2c1.h"

#define HMC5883_I2C_ADDR (0x1E)
#define HMC5883_CONFIG_A_REG (0x00)
#define HMC5883_CONFIG_B_REG (0x01)
#define HMC5883_MODE_REG (0x02)
#define HMC5883_DATA_REG (0x03)

typedef enum
{
	HMC5883_1_SAMPLE = 0x00 << 5, //default
	HMC5883_2_SAMPLES = 0x01 << 5,
	HMC5883_4_SAMPLES = 0x02 << 5,
	HMC5883_8_SAMPLES = 0x03 << 5
} hmc5883_samples_per_output_t;

typedef enum
{
	HMC5883_0_75Hz = 0x00 << 2,
	HMC5883_1_5Hz = 0x01 << 2,
	HMC5883_3Hz = 0x02 << 2,
	HMC5883_7_5Hz = 0x03 << 2,
	HMC5883_15Hz = 0x04 << 2, //default
	HMC5883_30Hz = 0x05 << 2,
	HMC5883_75Hz = 0x06 << 2
} hmc5883_output_rate_t;

typedef enum
{
	HMC5883_1370_Gain = 0x00,
	HMC5883_1090_Gain = 0x01, //default
	HMC5883_820_Gain = 0x02,
	HMC5883_660_Gain = 0x03,
	HMC5883_440_Gain = 0x04,
	HMC5883_390_Gain = 0x05,
	HMC5883_330_Gain = 0x06,
	HMC5883_230_Gain = 0x07
} hmc5883_gain_t;

typedef enum
{
	HMC5883_CONTINUOUS_MEASUREMENT_MODE = 0x00,
	HMC5883_SINGLE_MEASUREMENT_MODE = 0x01,
	HMC5883_IDLE_MODE = 0x02
} hmc_5883_operating_mode_t;

typedef struct
{
	int16_t x;
	int16_t z;
	int16_t y;
} hmc5883_t;

typedef struct
{
	hmc5883_samples_per_output_t samples_per_output;
	hmc5883_output_rate_t output_rate;
	hmc5883_gain_t gain;
} hmc5883_config_t;

void initHMC5883();
void configHMC5883(hmc5883_config_t config);
void startHMC5883();
void stopHMC5883();
void readHMC5883(hmc5883_t *data);

#ifdef	__cplusplus
}
#endif

#endif	/* HMC5883_H */

