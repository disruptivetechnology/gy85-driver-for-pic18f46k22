/* 
 * File:   ADXL345.h
 * Author: eluinstra
 *
 * Created on February 10, 2016, 7:48 PM
 */

#ifndef ADXL345_H
#define	ADXL345_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/i2c1.h"

#define ADXL345_I2C_ADDR (0x53)
#define ADXL345_BW_RATE_REG (0x2C)
#define ADXL345_POWER_CTL_REG (0x2D)
#define ADXL345_DATA_FORMAT_REG (0x31)
#define ADXL345_DATA_REG (0x32)
#define ADXL345_STANDBY_MODE (0x00)
#define ADXL345_MEASUREMENT_MODE (0x08)
#define ADXL345_LOW_POWER_MODE (0x10)
#define ADXL345_FULL_RES_MODE (0x08)
#define ADXL345_LEFT_JUSTIFIED_MODE (0x04)

typedef enum
{
	ADXL345_2G = 0x00,
	ADXL345_4G = 0x01,
	ADXL345_8G = 0x02,
	ADXL345_16G = 0x03
} adxl345_range_t;

typedef enum
{
	ADXL345_6_25Hz = 0x06,
	ADXL345_12_5Hz = 0x07,
	ADXL345_25Hz = 0x08,
	ADXL345_50Hz = 0x09,
	ADXL345_100Hz = 0x0A,
	ADXL345_200Hz = 0x0B,
	ADXL345_400Hz = 0x0C,
	ADXL345_800Hz = 0x0D,
	ADXL345_1600Hz = 0x0E,
	ADXL345_3200Hz = 0x0F
} adxl345_output_rate_t;

typedef struct
{
	int16_t x;
	int16_t y;
	int16_t z;
} adxl345_t;

typedef struct
{
	bool low_power_mode;
	adxl345_output_rate_t output_rate;
	bool full_res_mode;
	bool left_justified_mode;
	adxl345_range_t range;
} adxl345_config_t;

void initADXL345(adxl345_range_t range);
void configADXL345(adxl345_config_t config);
void startADXL345();
void stopADXL345();
void readADXL345(adxl345_t *data);

#ifdef	__cplusplus
}
#endif

#endif	/* ADXL345_H */

