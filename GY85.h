/* 
 * File:   GY85.h
 * Author: eluinstra
 *
 * Created on February 10, 2016, 7:53 PM
 */

#ifndef GY85_H
#define	GY85_H

#include "ADXL345.h"
#include "HMC5883.h"
#include "ITG3200.h"

#ifdef	__cplusplus
extern "C"
{
#endif

#define accelerometer_t adxl345_t
#define accelerometer_range_t adxl345_range_t
#define compass_t hmc5883_t
#define gyroscope_t itg3200_t
#define gyroscope_bandwidth_t itg3200_dlpf_cfg_t
#define readAccelerometer(accelerometer) readADXL345(accelerometer)
#define readCompass(compass) readHMC5883(compass)
#define readGyroscope(gyroscope) readITG3200(gyroscope)

typedef struct
{
	adxl345_config_t accelerometer;
	hmc5883_config_t compass;
	itg3200_config_t gyroscope;
} gy85_config_t;

typedef struct
{
	adxl345_t accelerometer;
	hmc5883_t compass;
	itg3200_t gyroscope;
} gy85_t;

void initGY85(accelerometer_range_t range, gyroscope_bandwidth_t bandwidth);
void configGY85(gy85_config_t config);
void startGY85();
void stopGY85();
void calibrateGY85();
void readGY85(gy85_t *data);

#ifdef	__cplusplus
}
#endif

#endif	/* GY85_H */

