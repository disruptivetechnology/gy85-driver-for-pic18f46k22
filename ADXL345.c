#include "ADXL345.h"

void initADXL345(adxl345_range_t range)
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	{
		uint8_t writeBuffer[] = {ADXL345_DATA_FORMAT_REG, 0x00};
		writeBuffer[1] = range;
		I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), ADXL345_I2C_ADDR, &status);
		while (status == I2C1_MESSAGE_PENDING);
	}
	{
		uint8_t writeBuffer[] = {ADXL345_POWER_CTL_REG, ADXL345_MEASUREMENT_MODE};
		I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), ADXL345_I2C_ADDR, &status);
		while (status == I2C1_MESSAGE_PENDING);
	}
}

void configADXL345(adxl345_config_t config)
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	{
		uint8_t reg = config.output_rate;
		if (config.low_power_mode)
		{
			reg |= ADXL345_LOW_POWER_MODE;
			if (config.output_rate > ADXL345_400Hz)
			{
				reg &= 0xF0;
				reg |= ADXL345_400Hz;
			}
		}
		uint8_t writeBuffer[] = {ADXL345_DATA_FORMAT_REG, 0x00};
		writeBuffer[1] = reg;
		I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), ADXL345_I2C_ADDR, &status);
		while (status == I2C1_MESSAGE_PENDING);
	}
	{
		uint8_t reg = config.range;
		if (config.full_res_mode)
			reg |= ADXL345_FULL_RES_MODE;
		if (config.left_justified_mode)
			reg |= ADXL345_LEFT_JUSTIFIED_MODE;
		uint8_t writeBuffer[] = {ADXL345_DATA_FORMAT_REG, 0x00};
		writeBuffer[1] = reg;
		I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), ADXL345_I2C_ADDR, &status);
		while (status == I2C1_MESSAGE_PENDING);
	}
}

void startADXL345()
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {ADXL345_POWER_CTL_REG, ADXL345_MEASUREMENT_MODE};
	I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), ADXL345_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
}

void stopADXL345()
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {ADXL345_POWER_CTL_REG, ADXL345_STANDBY_MODE};
	I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), ADXL345_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
}

void readADXL345(adxl345_t *data)
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {ADXL345_DATA_REG};
	I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), ADXL345_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
	I2C1_MasterRead((uint8_t *) data, sizeof (adxl345_t), ADXL345_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
}

