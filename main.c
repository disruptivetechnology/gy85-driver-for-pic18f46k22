/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB� Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC18F46K22
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#include "mcc_generated_files/mcc.h"
#include "GY85.h"

gy85_config_t gy85_config;
gy85_t gy85;

void delay_10ms(uint16_t n)
{
	while (n-- > 0)
		__delay_ms(10);
}

/*
                         Main application
 */
void main(void)
{
  // Initialize the device
  SYSTEM_Initialize();

  // Enable the Global Interrupts
  INTERRUPT_GlobalInterruptEnable();

  // Enable the Peripheral Interrupts
  INTERRUPT_PeripheralInterruptEnable();

	/*Clear screen and set cursor to the start of the current line*/
	printf("\033[2J\033[0;0H");
	printf("Microchip GY-85 Demo\r\n");

	gy85_config.accelerometer.low_power_mode = false;
	gy85_config.accelerometer.output_rate = ADXL345_100Hz;
	gy85_config.accelerometer.full_res_mode = false;
	gy85_config.accelerometer.left_justified_mode = false;
	gy85_config.accelerometer.range = ADXL345_4G;
	
	gy85_config.compass.samples_per_output = HMC5883_1_SAMPLE;
	gy85_config.compass.output_rate = HMC5883_15Hz;
	gy85_config.compass.gain = HMC5883_1090_Gain;
	
	gy85_config.gyroscope.sample_rate_divider = 0x05;
	gy85_config.gyroscope.dlpf_cfg = ITG3200_5Hz;
	
	configGY85(gy85_config);
	startGY85();
	delay_10ms(100);
	calibrateGY85();
	while (1)
	{
		readGY85(&gy85);
		printf("accelerometer x: %d, y: %d, z: %d\r\n", gy85.accelerometer.x, gy85.accelerometer.y, gy85.accelerometer.z);
		printf("compass x: %d, y: %d, z: %d\r\n", gy85.compass.x, gy85.compass.y, gy85.compass.z);
		printf("gyro x: %f, y: %f, z: %f\r\n", gy85.gyroscope.x, gy85.gyroscope.y, gy85.gyroscope.z);
		printf("temperature: %f\r\n", gy85.gyroscope.temp);
		delay_10ms(100);
	}
}
/**
 End of File
 */