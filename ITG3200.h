/* 
 * File:   ITG3200.h
 * Author: eluinstra
 *
 * Created on February 10, 2016, 7:50 PM
 */

#ifndef ITG3200_H
#define	ITG3200_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/i2c1.h"

#define ITG3200_I2C_ADDR (0x68)
#define ITG3200_SMPLRT_DIV_REG (0x15)
#define ITG3200_DLPF_FS_REG (0x16)
#define ITG3200_PWR_MGM_REG (0x3E)
#define ITG3200_DATA_REG (0x1B)
#define ITG3200_FS_SEL (0x03 << 3)
#define ITG3200_MEASUREMENT_MODE (0x00)
#define ITG3200_STANDBY_MODE (0x38)

typedef enum
{
	ITG3200_256Hz = 0x00,
	ITG3200_188Hz = 0x01,
	ITG3200_98Hz = 0x02,
	ITG3200_42Hz = 0x03,
	ITG3200_20Hz = 0x04,
	ITG3200_10Hz = 0x05,
	ITG3200_5Hz = 0x06
} itg3200_dlpf_cfg_t;

typedef struct
{
	int8_t sample_rate_divider;
	itg3200_dlpf_cfg_t dlpf_cfg;
} itg3200_config_t;

typedef struct
{
	float x;
	float y;
	float z;
	float temp;
} itg3200_t;

typedef struct
{
	int16_t x;
	int16_t y;
	int16_t z;
} itg3200_offset_t;

void initITG3200(itg3200_dlpf_cfg_t dlpf_cfg);
void configITG3200(itg3200_config_t config);
void startITG3200();
void stopITG3200();
void calibrateITG3200();
void readITG3200(itg3200_t *data);

#ifdef	__cplusplus
}
#endif

#endif	/* ITG3200_H */

