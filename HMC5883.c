#include "HMC5883.h"

void initHMC5883()
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {HMC5883_MODE_REG, HMC5883_CONTINUOUS_MEASUREMENT_MODE};
	I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), HMC5883_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
}

void configHMC5883(hmc5883_config_t config)
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	{
		uint8_t writeBuffer[] = {HMC5883_MODE_REG, HMC5883_IDLE_MODE};
		I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), HMC5883_I2C_ADDR, &status);
		while (status == I2C1_MESSAGE_PENDING);
	}
	{
		uint8_t writeBuffer[] = {HMC5883_CONFIG_A_REG, 0x00};
		writeBuffer[1] = config.samples_per_output | config.output_rate;
		I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), HMC5883_I2C_ADDR, &status);
		while (status == I2C1_MESSAGE_PENDING);
	}
	{
		uint8_t writeBuffer[] = {HMC5883_CONFIG_B_REG, 0x00};
		writeBuffer[1] = config.gain;
		I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), HMC5883_I2C_ADDR, &status);
		while (status == I2C1_MESSAGE_PENDING);
	}
}

void startHMC5883()
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {HMC5883_MODE_REG, HMC5883_CONTINUOUS_MEASUREMENT_MODE};
	I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), HMC5883_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
}

void stopHMC5883()
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {HMC5883_MODE_REG, HMC5883_IDLE_MODE};
	I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), HMC5883_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
}

void readHMC5883(hmc5883_t *data)
{
	I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {HMC5883_DATA_REG};
	I2C1_MasterWrite(writeBuffer, sizeof (writeBuffer), HMC5883_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
	I2C1_MasterRead((uint8_t *) data, sizeof (hmc5883_t), HMC5883_I2C_ADDR, &status);
	while (status == I2C1_MESSAGE_PENDING);
	data->x = ((data->x >> 8 & 0xFF) | (data->x << 8));
	data->y = ((data->y >> 8 & 0xFF) | (data->y << 8));
	data->z = ((data->z >> 8 & 0xFF) | (data->z << 8));
}

